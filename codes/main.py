#!/usr/bin/python

from ratings import Ratings
from directors import Director
import time
import sys

from mysql import MySQL
from log import Log

logger = Log()
logger.info("Logger Initialized")



database_name = "reelphil"
movie = open("movies.txt", "w")
series = open("series.txt", "w")
sql = MySQL(database_name)

tick = time.clock()
all_movie_list = Ratings(logger).start()
toc = time.clock()
print '\ntime for all movie list', (toc-tick)

print "\n\ntime for director"
tick = time.clock()
director_object = Director(logger)
director_movie_mapping = director_object.start()
toc = time.clock()
print 'time for all director movie list', (toc-tick)


print "tick"
tick = time.clock()
print "read"
ratings = Ratings(logger, 200)
top_voted_movie_list = ratings.start()
print "\ntoc"
toc = time.clock()
print 'time for top movie list', (toc-tick)

dummy = top_voted_movie_list.copy()


print "writting director and their all movies to database..."
print
print
for director in director_movie_mapping:
    try:
        if len(director_movie_mapping[director]) > 1:
            sys.stdout.write('.')
            director_object.insert_director(director)
            for movie in director_movie_mapping[director]:
                director_object.insert_director_movies(director, movie)
                if top_voted_movie_list.get(movie, None):
                    logger.info("%s not found" % movie)
                else:
                    logger.warning("%s not found" % movie)
                    new_movie = all_movie_list.get(movie)
                    if new_movie:
                        try:
                            ratings.insert(new_movie)
                        except Exception as e:
                            logger.error("%s not found" % movie)
                            try:
                                print "str error : ", e.strerror()
                            except:
                                try:
                                    print "value error : ", e.value()
                                except:
                                    print "simple error : ", e

                    else:
                        logger.error("%s not found" % movie)
            print
    except:
        try:
            logger.error("something error happened for director: %s mapping: %s" % (director, str(director_movie_mapping[director])))
        except:
            logger.error("something error happened for director: %s" % (director))
