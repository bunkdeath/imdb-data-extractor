import psycopg2

class MySQL:
    def __init__(self, database, logger=None):
        host = "127.0.0.1"
        username = "postgres"
        password = ""
        conn_string = "host='%s' dbname='%s' user='%s' password='%s'" % (host, database, username, password)
        self.conn = psycopg2.connect(conn_string)
        self.cursor = self.conn.cursor()
        
        if logger:
            self.logger = logger
    
    def query(self, query):
        try:
            self.cursor.execute(query)
            self.conn.commit()
        except:
            if self.logger:
                self.logger.error("Error in query : %s" % query)
            raise

    def get_cursor(self, query):
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def commit(self):
        self.conn.commit()
