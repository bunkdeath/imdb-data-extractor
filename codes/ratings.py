import os
import re
import sys

from slugify import slugify
from mysql import MySQL

class Ratings:
    def __init__(self, logger, voting_benchmark=None):
        self.logger = logger

        database_name = "reelphil"
        self.table_name = "movie"

#        database_name = "test-rating"
#        self.table_name = "test"

        self.data = {}
#        self.all_data = {}

        self.sql = MySQL(database_name)
        self.file = os.path.join(os.getcwd(), '..', 'gz', 'ratings.list.txt')

        self.sql_insert = True

        if voting_benchmark:
            self.sql_insert = True
            self.voting_benchmark = voting_benchmark
        else:
            self.sql_insert = False
            self.voting_benchmark = 0
        

    def insert(self, entry):
        movie_name = entry.get('movie_name')
        year_released = entry.get('year_released')
        rating = entry.get('rating')
        vote = entry.get('vote')

        self.data[movie_name] = entry

        if self.sql_insert:
            try:
                slugified_name = slugify(movie_name)
                slugified_name = self._generate_slug_for_db(slugified_name)

                #movie_name = re.escape(movie_name)
                movie_name = movie_name.replace("'", "''")
                movie_name = movie_name.replace("\\", "\\\\")
                #query = "INSERT INTO %s (title, year, rating, slug) VALUES('%s', '%s', '%s', '%s')" % (self.table_name, movie_name, year_released, rating, slugified_name)
                query = "INSERT INTO %s (title, year, rating, slug, vote) VALUES('%s', '%s', '%s', '%s', '%s')" % (self.table_name, movie_name, year_released, rating, slugified_name, vote)
            
            #try:
                self.sql.query(query)
            except:
                #print "%s query raised error\n%s\n------------------------------------------------------" % (query, line)
                try:
                    self.logger.error("%s : %s query raised error" % (entry.get('movie_name'), query))
                except:
                    self.logger.error("Entry raised error : " + str(entry))

    def start(self):
        '''
        returns list of dictionary containing key->value of films and its data
        eg.
        {
            "movie title": {
                                "movie_name": "movie title",
                                "year_released": "year of release of film",
                                "rating": "rating of the movie",
                                "vote": "total vote got by the movie"
                            },
            ...
        }
        '''
        print

        self._create_first_file()
        self._create_second_file()
        self._parse_movies_to_database()
        #self.sql.commit()
        return self.data

    def _create_first_file(self):
        start = False
        highest_ratings = open('highest-ratings.txt', 'w')

        lines = open(self.file, "r").read().splitlines()
        for line in lines:
            if start:
                start = True
                toks = self.split_first(line, 3)
                try:
                    vote = int(toks[1])
#                    rating = float(toks[2])
                    if vote >= self.voting_benchmark:
                        if len(toks) > 3:
                            line = "%s %s %s\n" % (toks[1], toks[2], toks[3])
                            highest_ratings.write(line)
                except:
#                    print 'error in parsing "%s"' % line
                    continue

            if line == "New  Distribution  Votes  Rank  Title":
                start = True


    def _create_second_file(self):
        movies = open('rating-movie.txt', 'w')
        series = open('rating-series.txt', 'w')

        lines = open('highest-ratings.txt', 'r').read().splitlines()

        for line in lines:
            if '"' in line and '{' in line:
                series.write(line)
                series.write('\n')
            else:
                movies.write(line)
                movies.write('\n')


        movies.close()
        series.close()



    def _parse_movies_to_database(self):
        no_year = open('no-years.txt', 'w')
        lines = open('rating-movie.txt', 'r').read().splitlines()
        
        for line in lines:
            sys.stdout.write('.')
            vote, rating, rest = self.split_first(line, 2)
            
            if rest.find('(TV)') != -1:
                continue
            if rest.find('V') != -1:
                continue
            if rest.find('VG') != -1:
                continue
            #rest = rest.replace('(TV)', '').strip()
            #rest = rest.replace('(V)', '').strip()
            #rest = rest.replace('(VG)', '').strip()

            try:
                movie_name = rest[:-7]
            except:
#                print '%s has no movie name' % line
                continue

            try:
                year_released = rest[-6:].replace('(', '').replace(')', '')
                if len(year_released) < 4:
                    continue
            except:
#                print "%s has no release year" % line
                continue

            try:
                year_released = int(year_released)
            except:
#                print "%s to file" % line
                no_year.write(line)
                no_year.write('\n')
                continue

            entry = {
                'movie_name': movie_name,
                'year_released': year_released,
                'rating': rating,
                'vote': vote
            }

            self.insert(entry)

        no_year.close()

    def _add_year_in_slug(self, slug):
        pass
    
    def _add_number_in_slug(self, slug):
        pass
    
    def _generate_slug_for_db(self, slug, add_number=None):
        #number = 2
        #if add_number:
        #    slug = slug[:slug.rfind('-')]
        #    slug += '-' + str(add_number)
        #    query = "SELECT * FROM %s WHERE slug='%s'" % (self.table_name, slug)
        #    number = add_number + 1
        #else:
        #    query = "SELECT * FROM %s WHERE slug='%s'" % (self.table_name, slug)
        #
        #result = self.sql.get_cursor(query)
        #if result:
        #    if not add_number:
        #        result = result[0]
        #        id, title, year, rating, runtime, slug = result
        #        slug = slug + '-' + str(year)
        #
        #    slug = self._generate_slug_for_db(slug, add_number=number)
        #return slug
        
        query = "SELECT * FROM %s WHERE slug='%s'" % (self.table_name, slug)
        result = self.sql.get_cursor(query)
        if result:
            result = result[0]
            id, title, year, rating, runtime, slug = result
            slug = slug + '-' + str(year)
        else:
            return slug
        
        number = 2
        intermediate_slug = slug
        while(True):
            query = "SELECT * FROM %s WHERE slug='%s'" % (self.table_name, intermediate_slug)
            result = self.sql.get_cursor(query)
            if result:
                intermediate_slug = slug + '-' + str(number)
                number += 1
            else:
                break
        
        return intermediate_slug

    def split_first(self, string, count):
        return string.split(None, count)

#def split_first(string, count):
##    distribution, votes, rating, title = string.split(None, count)
#    return string.split(None, count)
#
#
#rating_benchmark = 7
#
#
#
#def create_first_file():
#    highest_ratings = open('highest-ratings.txt', 'w')
#
#    i = 0
#    start = False
#
#    lines = open("ratings.list.txt", "r").read().splitlines()
#    for line in lines:
#        if start:
#            start = True
#            toks = split_first(line, 3)
#            try:
#                rating = float(toks[2])
#                if rating >= rating_benchmark:
#                    if len(toks) > 3:
#                        line = "%s %s\n" % (toks[2], toks[3])
#                        highest_ratings.write(line)
#            except:
#                print 'error in parsing "%s"' % line
#                continue
#
#        if line == "New  Distribution  Votes  Rank  Title":
#            start = True
#
#def create_second_file():
#    movies = open('rating-movie.txt', 'w')
#    series = open('rating-series.txt', 'w')
#
#
#    lines = open('highest-ratings.txt', 'r').read().splitlines()
#
#    for line in lines:
#        if '"' in line and '{' in line:
#            series.write(line)
#            series.write('\n')
#        else:
#            movies.write(line)
#            movies.write('\n')
#
#
#    movies.close()
#    series.close()
#
#
#def parse_movies_to_database():
#
#    database_name = "reelphi"
#    sql = MySQL(database_name)
#
#    no_year = open('no-years.txt', 'w')
#
#    lines = open('rating-movie.txt', 'r').read().splitlines()
#
#    for line in lines:
#        rating, rest = split_first(line, 1)
#
#
#
#        rest = rest.replace('(TV)', '').strip()
#        rest = rest.replace('(V)', '').strip()
#        rest = rest.replace('(VG)', '').strip()
#
#
#
#        try:
#            movie_name = rest[:-7]
#        except:
#            print '%s has no movie name' % line
#            continue
#
#        try:
#            year_released = rest[-6:].replace('(', '').replace(')', '')
#            if len(year_released) < 4:
#                continue
#        except:
#            print "%s has no release year" % line
#            continue
#
#        try:
#            year_released = int(year_released)
#        except:
#            print "%s to file" % line
#            no_year.write(line)
#            no_year.write('\n')
#            continue
#
#        slugified_name = slugify(movie_name)
#
##        print rating, movie_name, slugified_name
#        movie_name = re.escape(movie_name)
#        query = 'INSERT INTO test (title, year, rating, slug) VALUES("%s", "%s", "%s", "%s")' % (movie_name, year_released, rating, slugified_name)
#        try:
##            print "%s\n------------------------------------------------------" % (query)
#            sql.query(query)
#        except:
#            print "%s query raised error\n%s\n------------------------------------------------------" % (query, line)

        

#create_first_file()
#create_second_file()
#parse_movies_to_database()

#rating = Ratings()
#rating.start()
