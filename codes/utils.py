

def is_series(line, director_name=None):
    if '(#' in str(line):
        if director_name:
            if 'Ordish, Roger' in director_name:
                print "return True"
        return True
    else:
        if director_name:
            if 'Ordish, Roger' in director_name:
                print "return False"
        return False

def get_movie_title(line):
    def cut_from_last_open_braket(line):
        index = line.rfind('(')
        return line[:index].strip()

    line = line.strip()
    while line[-1:] == ')':
        line = cut_from_last_open_braket(line)
    line = line.strip()
    if line.count('"') == 2:
        if line[0] == '"' and line[-1] == '"':
            line = line.replace('"', '')
    elif line.count('"') > 0:
        line = line.replace('"', '\\"')

    return line
