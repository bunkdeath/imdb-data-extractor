from utils import is_series
from utils import get_movie_title

from mysql import MySQL

import re
from slugify import slugify

import os
import sys


class Director:
    def __init__(self, logger):
        self.logger = logger
        database_name = "reelphil"

#        database_name = "test-rating"

        self.sql = MySQL(database_name)
        self.file = os.path.join(os.getcwd(), '..', 'gz', 'directors.list.txt')

        self.data = {}

    def start(self):
        self.all = {}
        self._read()

        return self.data

    def _read(self):
#        i = 0
#        LIMIT = 10
        start = False
        delimiter_line = "----			------"
        film_list = []
        director_name = ""
        lines = open(self.file, 'r').read().splitlines()
        for line in lines:
            sys.stdout.write('.')
            if start:
#                print line
                tokens = line.split('\t')
                if tokens[0]:
                    '''
                    contains director's name
                    '''

                    if film_list:
                        '''
                        if film_list is not empty, best guess is the list is from
                        director that was read previously
                        '''
#                        print director_name, film_list
                        self.data[director_name] = film_list
                        #empty film_list for new director
                        film_list = []

                    director_name = tokens[0]
                    try:
                        if tokens[1]:
                            title = tokens[1]
                        else:
                            title = tokens[2]

                        if not is_series(title):
                            film_list.append(get_movie_title(title))
                    except:
#                        print "Something wrong in this line : %s " % line
                        pass
                else:
                    '''
                    contains film list from previously read director name
                    '''
                    for token in tokens:
                        if token:
                            if not is_series(token):
                                film_list.append(get_movie_title(token))


            if line == delimiter_line:
                start = True
        self.data[director_name] = film_list


    def get_movie_id(self, movie_title):
        try:
            query = "SELECT id FROM movie WHERE title='%s'" % movie_title.replace("'", "''").replace("\\", "\\\\")
    #        print query
            result = self.sql.get_cursor(query)
    #        print result
            if result and len(result) == 1:
                id = result[0][0]
    #            print id
                return id
            else:
    #            print "multiple entry for movie title '%s'" % movie_title
                self.logger.error("multiple/no entry for movie '%s'" % movie_title)
        except:
            self.logger.error("Error in movie title: '%s'; query: '%s'" % (movie_title, query))
            
    def get_director_id(self, director_name):
        try:
            query = "SELECT id FROM person WHERE name='%s'" % director_name.replace("'", "''").replace("\\", "\\\\")
#           print query
            result = self.sql.get_cursor(query)
#        print result
            if result and len(result) == 1:
                id = result[0][0]
#               print id
                return id
            else:
#               print "multiple entry for person name '%s'" % director_name
                self.logger.error("multiple/no entry for person name '%s'" % director_name)
        except:
            self.logger.error("multiple/no entry for person name '%s'" % director_name)
            return None
        return None

    def get_movie_id(self, movie_title):
        try:
            query = "SELECT id FROM movie WHERE title='%s'" % movie_title.replace("'", "''").replace("\\", "\\\\")
    #        print query
            result = self.sql.get_cursor(query)
    #        print result
            if result and len(result) == 1:
                id = result[0][0]
    #            print id
                return id
            else:
    #            print "multiple/no entry for movie title name '%s'" % movie_title
                self.logger.error("multiple/no entry for movie title name '%s'" % movie_title)
        except:
            self.logger.error("multiple/no entry for movie title name '%s'" % movie_title)

        return None

    def insert_director(self ,director_name):
        try:
            self.table_name = "person"
            slugified_name = slugify(director_name)
            slugified_name = self._generate_slug_for_db(slugified_name)

            director_name = director_name.replace("'", "''").replace("\\", "\\\\")
            query = "INSERT INTO %s (name, slug) VALUES('%s', '%s')" % (self.table_name, director_name, slugified_name)
        #try:
            self.sql.query(query)
        except:
#            print "%s query raised error\n%s\n------------------------------------------------------" % (query, director_name)
            try:
                self.logger.error("%s query raised error\n%s" % (query, director_name))
            except:
                self.logger.error("Entry has error : %s" % str(director_name))
    
    def insert_director_movies(self, director_name, movie_title):
        director_id = self.get_director_id(director_name)
        movie_id = self.get_movie_id(movie_title)

        query = "INSERT INTO movie_directors (movie_id, person_id) VALUES (%s, %s)" % (director_id, movie_id)

        if director_id and movie_id:
            try:
                self.sql.query(query)
            except:
#                print "%s query raised error\n%s\n%s\n------------------------------------------------------" % (query, director_name, movie_title)
                self.logger.error("%s query raised error\n%s\n%s" % (query, director_name, movie_title))
        else:
            
#            print "could not write to db. incomplete data"
            self.logger.error("could not write to db. incomplete data; movie_id: %s; movie_title: %s, director_id: %s, director_name: %s" % (movie_id, movie_title, director_id, director_name))


    def _generate_slug_for_db(self, slug, add_number=1):
        number = 2
        query = "SELECT * FROM %s WHERE slug='%s'" % (self.table_name, slug)

        result = self.sql.get_cursor(query)
        if result:
#            print slug
            slug = slug[:slug.rfind('-')]
            slug += '-' + str(add_number)
            query = "SELECT * FROM %s WHERE slug='%s'" % (self.table_name, slug)
            number = add_number + 1

            slug = self._generate_slug_for_db(slug, add_number=number)
        return slug

#director = Director()
#director.start()
